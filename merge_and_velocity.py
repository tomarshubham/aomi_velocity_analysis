import pandas as pd


# merging the data frame of sales and stock.
def func_merge(file_1,file_2):
    df_sale = pd.read_csv(file_1)
    df_stock = pd.read_csv(file_2)

    df_sale = df_sale.rename(columns={'qty':'sale_unit'})
    df_stock = df_stock.rename(columns={'qty': 'stock_unit', 'entry_date': 'bill_date'})

    df = pd.merge(df_sale,df_stock,on=['bill_date', 'item_no'])
    return df


# velocity analysis function.
def analysis_vel(file_1, file_2):
    df = func_merge(file_1, file_2)

    print('Table before velocity analysis :')
    print(df)
    print()
    k, c = df.shape        # to get the no. of rows of data frame.
    df['units_remained'] = 0   # unit is in integer
    df['velocity'] = 0.0       # velocity is in float
    df['units_remained'] = df['stock_unit'] - df['sale_unit']

    for i in range(0, k):
        if i == 0:
            df.at[i, 'velocity'] = (df.at[i, 'sale_unit'] / df.at[i, 'stock_unit'])*100

        elif i != 0:
            if (df.at[i, 'item_no'] == df.at[i-1, 'item_no']) & (df.at[i, 'store_name_x'] == df.at[i-1, 'store_name_x']) :
                df.at[i, 'units_remained'] = df.at[i, 'stock_unit'] - df.at[i, 'sale_unit'] + df.at[
                    i - 1, 'units_remained']
                t = df.at[i, 'sale_unit'] / (df.at[i, 'stock_unit'] + df.at[i-1, 'units_remained'])
                df.at[i, 'velocity'] = t*100

            else:
                df.at[i, 'velocity'] = (df.at[i, 'sale_unit'] / df.at[i, 'stock_unit'])*100

    print('Table after Velocity analysis :')
    print(df)
    df.to_csv('sales_stock_dup.csv', encoding='utf-8')


if __name__ == '__main__':
    sale_file = 'v_sales_dup.csv'
    stock_file = 'v_stock_dup.csv'
    analysis_vel(sale_file, stock_file)
